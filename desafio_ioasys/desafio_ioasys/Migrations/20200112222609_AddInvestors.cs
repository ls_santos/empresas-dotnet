﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace desafio_ioasys.Migrations
{
    public partial class AddInvestors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Owner",
                table: "Enterprises");

            migrationBuilder.AddColumn<long>(
                name: "OwnerId",
                table: "Enterprises",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "InvestorsLogins",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InvestorId = table.Column<long>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvestorsLogins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Investors",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    LoginId = table.Column<long>(nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(9,2)", nullable: false),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    FirstAccess = table.Column<bool>(nullable: false),
                    SuperAngel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Investors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Investors_InvestorsLogins_LoginId",
                        column: x => x.LoginId,
                        principalTable: "InvestorsLogins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enterprises_OwnerId",
                table: "Enterprises",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Investors_LoginId",
                table: "Investors",
                column: "LoginId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Enterprises_Investors_OwnerId",
                table: "Enterprises",
                column: "OwnerId",
                principalTable: "Investors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enterprises_Investors_OwnerId",
                table: "Enterprises");

            migrationBuilder.DropTable(
                name: "Investors");

            migrationBuilder.DropTable(
                name: "InvestorsLogins");

            migrationBuilder.DropIndex(
                name: "IX_Enterprises_OwnerId",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Enterprises");

            migrationBuilder.AddColumn<string>(
                name: "Owner",
                table: "Enterprises",
                nullable: false,
                defaultValue: "");
        }
    }
}
