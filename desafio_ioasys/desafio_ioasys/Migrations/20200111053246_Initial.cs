﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace desafio_ioasys.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnterprisesSocialMedias",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EnterpriseId = table.Column<long>(nullable: false),
                    Facebook = table.Column<string>(nullable: true),
                    Twitter = table.Column<string>(nullable: true),
                    LinkedIn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterprisesSocialMedias", x => x.Id);
                    table.UniqueConstraint("AK_EnterprisesSocialMedias_EnterpriseId", x => x.EnterpriseId);
                });

            migrationBuilder.CreateTable(
                name: "EnterprisesTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterprisesTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enterprises",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    SocialMediaId = table.Column<long>(nullable: true),
                    Phone = table.Column<string>(nullable: false),
                    Owner = table.Column<string>(nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: true),
                    Shares = table.Column<int>(nullable: true),
                    SharePrice = table.Column<decimal>(type: "decimal(7,2)", nullable: true),
                    City = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    TypeId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enterprises_EnterprisesSocialMedias_SocialMediaId",
                        column: x => x.SocialMediaId,
                        principalTable: "EnterprisesSocialMedias",
                        principalColumn: "EnterpriseId",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Enterprises_EnterprisesTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "EnterprisesTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enterprises_SocialMediaId",
                table: "Enterprises",
                column: "SocialMediaId",
                unique: true,
                filter: "[SocialMediaId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Enterprises_TypeId",
                table: "Enterprises",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enterprises");

            migrationBuilder.DropTable(
                name: "EnterprisesSocialMedias");

            migrationBuilder.DropTable(
                name: "EnterprisesTypes");
        }
    }
}
