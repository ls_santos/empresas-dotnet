﻿namespace desafio_ioasys.Models.ViewModels.v1
{
    public class EnterpriseFilter
    {
        public long? Enterprise_types { get; set; }
        public string Name { get; set; }
    }
}
