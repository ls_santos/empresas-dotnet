﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using System.Collections.Generic;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class EnterpriseResult
    {
        public long Id { get; set; }
        public string Enterprise_name { get; set; }
        public string Description { get; set; }
        public string Email_enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool Own_enterprise { get; set; }
        public string Photo { get; set; }
        public int Value { get; set; }
        public int Shares { get; set; }
        public decimal Share_price { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public EnterpriseTypeResult Enterprise_type { get; set; }

        public EnterpriseResult(Enterprise enterprise, List<Enterprise> investorEnterprises)
        {
            Id = enterprise.Id;
            Enterprise_name = enterprise.Name;
            Description = enterprise.Description;
            Email_enterprise = enterprise.Email;
            Facebook = enterprise.SocialMedia?.Facebook;
            Twitter = enterprise.SocialMedia?.Twitter;
            Linkedin = enterprise.SocialMedia?.LinkedIn;
            Phone = enterprise.Phone;
            Own_enterprise = investorEnterprises.Contains(enterprise);
            Photo = enterprise.Photo;
            Value = enterprise.Value ?? 0;
            Shares = enterprise.Shares ?? 0;
            Share_price = enterprise.SharePrice ?? 0;
            City = enterprise.City;
            Country = enterprise.Country;
            Enterprise_type = new EnterpriseTypeResult(enterprise.Type);
        }
    }
}
