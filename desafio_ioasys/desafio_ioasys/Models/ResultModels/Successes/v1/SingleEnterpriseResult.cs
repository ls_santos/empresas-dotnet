﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using System.Collections.Generic;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class SingleEnterpriseResult 
    {
        public EnterpriseResult Enterprise { get; set; }
        public bool Success { get; set; }

        public SingleEnterpriseResult(Enterprise enterprise, List<Enterprise> investorEnterprises)
        {
            Enterprise = new EnterpriseResult(enterprise, investorEnterprises);
            Success = true;
        }
    }
}
