﻿using desafio_ioasys.Models.EntityModels.Investors;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class SignInResult
    {
        public InvestorResult Investor { get; set; }
        public EnterpriseResult Enterprise { get; set; }
        public bool Success { get; set; }

        public SignInResult(Investor investor)
        {
            Investor = new InvestorResult(investor);
            Enterprise = null;
            Success = true;
        }
    }
}
