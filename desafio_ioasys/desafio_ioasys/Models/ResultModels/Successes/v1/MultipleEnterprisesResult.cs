﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using System.Collections.Generic;
using System.Linq;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class MultipleEnterprisesResult
    {
        public List<EnterpriseResult> Enterprises { get; set; }

        public MultipleEnterprisesResult(List<Enterprise> enterprises, List<Enterprise> investorEnterprises)
        {
            Enterprises = enterprises.Select(enterprise => new EnterpriseResult(enterprise, investorEnterprises)).ToList();
        }
    }
}
