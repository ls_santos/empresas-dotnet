﻿using desafio_ioasys.Models.EntityModels.Investors;
using System.Linq;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class InvestorResult
    {
        public long Id { get; set; }
        public string Investor_name { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }
        public PortfolioResult Portfolio { get; set; }
        public decimal Portfolio_value { get; set; }
        public bool First_access { get; set; }
        public bool Super_angel { get; set; }

        public InvestorResult(Investor investor)
        {
            Id = investor.Id;
            Investor_name = investor.Name;
            Email = investor.Login.Email;
            City = investor.City;
            Country = investor.Country;
            Balance = investor.Balance;
            Photo = investor.Photo;
            Portfolio = new PortfolioResult(investor.Enterprises.ToList());
            Portfolio_value = 0;
            First_access = investor.FirstAccess;
            Super_angel = investor.SuperAngel;
        }
    }
}
