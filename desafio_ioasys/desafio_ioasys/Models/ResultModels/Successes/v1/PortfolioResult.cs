﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using desafio_ioasys.Models.EntityModels.Investors;
using System.Collections.Generic;
using System.Linq;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class PortfolioResult
    {
        public int Enterprises_number { get; set; }
        public List<EnterpriseResult> Enterprises { get; set; }

        public PortfolioResult(List<Enterprise> enterprises)
        {
            if (enterprises != null && enterprises.Any())
            {
                Enterprises_number = enterprises.Count;
                Enterprises = enterprises.Select(enterprise => new EnterpriseResult(enterprise, enterprises)).ToList();
            }
            else
            {
                Enterprises_number = 0;
            }
        }
    }
}
