﻿using desafio_ioasys.Models.EntityModels.EnterprisesTypes;

namespace desafio_ioasys.Models.ResultModels.Successes.v1
{
    public class EnterpriseTypeResult
    {
        public long Id { get; set; }
        public string Enterprise_type_name { get; set; }

        public EnterpriseTypeResult(EnterpriseType type)
        {
            Id = type.Id;
            Enterprise_type_name = type.Name;
        }
    }
}
