﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace desafio_ioasys.Models.EntityModels.Enterprises
{
    public static class EnterpriseQuery
    {
        public static IQueryable<Enterprise> WhereId(this IQueryable<Enterprise> enterprises, long id)
            => enterprises.Where(enterprise => enterprise.Id == id);

        public static IQueryable<Enterprise> IncludeType(this IQueryable<Enterprise> enterprises)
            => enterprises.Include(p => p.Type);

        public static IQueryable<Enterprise> WhereTypeId(this IQueryable<Enterprise> enterprises, long? typeId)
        {
            if (typeId == null) return enterprises;

            return enterprises.Where(enterprise => enterprise.TypeId == typeId);
        }

        public static IQueryable<Enterprise> WhereName(this IQueryable<Enterprise> enterprises, string name)
        {
            if (name == null) return enterprises;

            return enterprises.Where(enterprise => enterprise.Name.Contains(name));
        }
    }
}
