﻿using desafio_ioasys.Models.EntityModels.EnterprisesSocialMedias;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desafio_ioasys.Models.EntityModels.Enterprises
{
    public static class EnterpriseMap
    {
        public static void Map(this EntityTypeBuilder<Enterprise> entity)
        {
            entity.ToTable("Enterprises");
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Id).UseSqlServerIdentityColumn();
            entity.Property(p => p.Name).IsRequired();
            entity.Property(p => p.Description).IsRequired();
            entity.Property(p => p.Email).IsRequired();
            entity.Property(p => p.SocialMediaId);
            entity.Property(p => p.Phone).IsRequired();
            entity.Property(p => p.OwnerId).IsRequired();
            entity.Property(p => p.Photo);
            entity.Property(p => p.Value);
            entity.Property(p => p.Shares);
            entity.Property(p => p.SharePrice).HasColumnType("decimal(7,2)");
            entity.Property(p => p.City).IsRequired();
            entity.Property(p => p.Country).IsRequired();
            entity.Property(p => p.TypeId).IsRequired();

            entity.HasOne(p => p.Type).WithMany(p => p.Enterprises).HasForeignKey(p => p.TypeId).OnDelete(DeleteBehavior.Restrict);
            entity.HasOne(p => p.SocialMedia).WithOne(p => p.Enterprise).HasForeignKey<Enterprise>(p => p.SocialMediaId)
                .HasPrincipalKey<EnterpriseSocialMedia>(p => p.EnterpriseId).OnDelete(DeleteBehavior.SetNull);
            entity.HasOne(p => p.Owner).WithMany(p => p.Enterprises).HasForeignKey(p => p.OwnerId).HasPrincipalKey(p => p.Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
