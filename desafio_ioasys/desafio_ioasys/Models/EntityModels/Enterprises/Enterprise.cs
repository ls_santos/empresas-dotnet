﻿using desafio_ioasys.Models.EntityModels.EnterprisesSocialMedias;
using desafio_ioasys.Models.EntityModels.EnterprisesTypes;
using desafio_ioasys.Models.EntityModels.Investors;

namespace desafio_ioasys.Models.EntityModels.Enterprises
{
    public class Enterprise
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public long? SocialMediaId { get; set; }
        public string Phone { get; set; }
        public long OwnerId { get; set; }
        public string Photo { get; set; }
        public int? Value { get; set; }
        public int? Shares { get; set; }
        public decimal? SharePrice { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public long TypeId { get; set; }
        public EnterpriseType Type { get; set; }
        public EnterpriseSocialMedia SocialMedia { get; set; }
        public Investor Owner { get; set; }
    }
}
