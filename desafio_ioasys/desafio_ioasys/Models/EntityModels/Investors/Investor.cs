﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using desafio_ioasys.Models.EntityModels.InvestorsLogins;
using System.Collections.Generic;

namespace desafio_ioasys.Models.EntityModels.Investors
{
    public class Investor
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long LoginId { get; set; }
        public decimal Balance { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Photo { get; set; }
        public bool FirstAccess { get; set; }
        public bool SuperAngel { get; set; }
        public ICollection<Enterprise> Enterprises { get; set; }
        public InvestorLogin Login { get; set; }
    }
}
