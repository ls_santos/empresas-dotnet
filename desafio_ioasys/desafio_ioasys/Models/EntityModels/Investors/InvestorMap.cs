﻿using desafio_ioasys.Models.EntityModels.InvestorsLogins;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desafio_ioasys.Models.EntityModels.Investors
{
    public static class InvestorMap
    {
        public static void Map(this EntityTypeBuilder<Investor> entity)
        {
            entity.ToTable("Investors");
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Id).UseSqlServerIdentityColumn();
            entity.Property(p => p.Name).IsRequired();
            entity.Property(p => p.LoginId).IsRequired();
            entity.Property(p => p.Balance).HasColumnType("decimal(9,2)");
            entity.Property(p => p.City);
            entity.Property(p => p.Country);
            entity.Property(p => p.Photo);
            entity.Property(p => p.FirstAccess);
            entity.Property(p => p.SuperAngel);

            entity.HasOne(p => p.Login).WithOne(p => p.Investor)
                .HasForeignKey<Investor>(p => p.LoginId)
                .HasPrincipalKey<InvestorLogin>(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
