﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace desafio_ioasys.Models.EntityModels.Investors
{
    public static class InvestorQuery
    {
        public static IQueryable<Investor> WhereId(this IQueryable<Investor> investors, long id)
            => investors.Where(investor => investor.Id == id);

        public static IQueryable<Investor> IncludeEnterprises(this IQueryable<Investor> investors)
            => investors.Include(investor => investor.Enterprises).ThenInclude(enterprise => enterprise.Type);

        public static IQueryable<Investor> IncludeLogin(this IQueryable<Investor> investors)
            => investors.Include(investor => investor.Login);

        public static IQueryable<Investor> WhereEmail(this IQueryable<Investor> investors, string email)
            => investors.Where(investor => investor.Login.Email == email);
    }
}
