﻿using desafio_ioasys.Models.EntityModels.Investors;

namespace desafio_ioasys.Models.EntityModels.InvestorsLogins
{
    public class InvestorLogin
    {
        public long Id { get; set; }
        public long InvestorId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Investor Investor { get; set; }
    }
}
