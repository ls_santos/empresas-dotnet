﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desafio_ioasys.Models.EntityModels.InvestorsLogins
{
    public static class InvestorLoginMap
    {
        public static void Map(this EntityTypeBuilder<InvestorLogin> entity)
        {
            entity.ToTable("InvestorsLogins");
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Id).UseSqlServerIdentityColumn();
            entity.Property(p => p.InvestorId).IsRequired();
            entity.Property(p => p.Email).IsRequired();
            entity.Property(p => p.Password).IsRequired();
        }
    }
}
