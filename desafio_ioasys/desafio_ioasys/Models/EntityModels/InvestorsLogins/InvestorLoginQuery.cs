﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace desafio_ioasys.Models.EntityModels.InvestorsLogins
{
    public static class InvestorLoginQuery
    {
        public static IQueryable<InvestorLogin> WhereEmail(this IQueryable<InvestorLogin> investorsLogins, string email)
            => investorsLogins.Where(investorLogin => investorLogin.Email == email);

        public static IQueryable<InvestorLogin> WherePassword(this IQueryable<InvestorLogin> investorsLogins, string password)
            => investorsLogins.Where(investorLogin => investorLogin.Password == password);

        public static IQueryable<InvestorLogin> IncludeInvestor(this IQueryable<InvestorLogin> investorsLogins)
            => investorsLogins.Include(investorLogin => investorLogin.Investor);
    }
}
