﻿using desafio_ioasys.Models.EntityModels.Enterprises;

namespace desafio_ioasys.Models.EntityModels.EnterprisesSocialMedias
{
    public class EnterpriseSocialMedia
    {
        public long Id { get; set; }
        public long EnterpriseId { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string LinkedIn { get; set; }
        public Enterprise Enterprise { get; set; }
    }
}
