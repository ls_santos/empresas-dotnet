﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desafio_ioasys.Models.EntityModels.EnterprisesSocialMedias
{
    public static class EnterpriseSocialMediaMap
    {
        public static void Map(this EntityTypeBuilder<EnterpriseSocialMedia> entity)
        {
            entity.ToTable("EnterprisesSocialMedias");
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Id).UseSqlServerIdentityColumn();
            entity.Property(p => p.EnterpriseId).IsRequired();
            entity.Property(p => p.Facebook);
            entity.Property(p => p.LinkedIn);
            entity.Property(p => p.Twitter);
        }
    }
}
