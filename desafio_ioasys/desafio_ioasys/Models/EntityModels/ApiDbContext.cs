﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using desafio_ioasys.Models.EntityModels.EnterprisesSocialMedias;
using desafio_ioasys.Models.EntityModels.EnterprisesTypes;
using desafio_ioasys.Models.EntityModels.Investors;
using desafio_ioasys.Models.EntityModels.InvestorsLogins;
using Microsoft.EntityFrameworkCore;

namespace desafio_ioasys.Models.EntityModels
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options) { }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseSocialMedia> EnterprisesSocialMedias { get; set; }
        public DbSet<EnterpriseType> EnterprisesTypes { get; set; }
        public DbSet<Investor> Investors { get; set; }
        public DbSet<InvestorLogin> InvestorsLogins { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Enterprise>().Map();
            modelBuilder.Entity<EnterpriseSocialMedia>().Map();
            modelBuilder.Entity<EnterpriseType>().Map();
            modelBuilder.Entity<Investor>().Map();
            modelBuilder.Entity<InvestorLogin>().Map();
        }
    }
}
