﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace desafio_ioasys.Models.EntityModels.EnterprisesTypes
{
    public static class EnterpriseTypeMap
    {
        public static void Map(this EntityTypeBuilder<EnterpriseType> entity)
        {
            entity.ToTable("EnterprisesTypes");
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Id).UseSqlServerIdentityColumn();
            entity.Property(p => p.Name).IsRequired();
        }
    }
}
