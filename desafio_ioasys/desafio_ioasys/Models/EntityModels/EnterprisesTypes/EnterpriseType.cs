﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using System.Collections.Generic;

namespace desafio_ioasys.Models.EntityModels.EnterprisesTypes
{
    public class EnterpriseType
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ICollection<Enterprise> Enterprises { get; set; }
    }
}
