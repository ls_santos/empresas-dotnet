﻿using desafio_ioasys.Models.EntityModels;
using desafio_ioasys.Models.EntityModels.Enterprises;
using desafio_ioasys.Models.ResultModels.Successes.v1;
using desafio_ioasys.Models.ViewModels.v1;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_ioasys.Services.Enterprises
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly ApiDbContext _dbContext;

        public EnterpriseService(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public MultipleEnterprisesResult MultipleEnterprisesResult { get; set; }
        public SingleEnterpriseResult SingleEnterpriseResult { get; set; }

        public async Task<bool> GetEnterprises(long? id, EnterpriseFilter filter, List<Enterprise> investorEnterprises)
        {
            if (filter.Name != null || filter.Enterprise_types != null)
            {
                var enterprises = await _dbContext.Enterprises
                    .WhereTypeId(filter.Enterprise_types)
                    .WhereName(filter.Name)
                    .IncludeType()
                    .ToListAsync();

                MultipleEnterprisesResult = new MultipleEnterprisesResult(enterprises, investorEnterprises);

                return true;
            }
            else if (id == null)
            {
                var enterprises = await _dbContext.Enterprises.IncludeType().ToListAsync();

                if (enterprises.Any())
                {
                    MultipleEnterprisesResult = new MultipleEnterprisesResult(enterprises, investorEnterprises);

                    return true;
                }
                else
                    return false;
            }
            else
            {
                var enterprise = await _dbContext.Enterprises
                    .WhereId(id.Value)
                    .IncludeType()
                    .SingleOrDefaultAsync();

                if (enterprise != null)
                {
                    SingleEnterpriseResult = new SingleEnterpriseResult(enterprise, investorEnterprises);

                    return true;
                }
                else
                    return false;
            }
        }
    }
}
