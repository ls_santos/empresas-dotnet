﻿using desafio_ioasys.Models.EntityModels.Enterprises;
using desafio_ioasys.Models.ResultModels.Successes.v1;
using desafio_ioasys.Models.ViewModels.v1;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace desafio_ioasys.Services.Enterprises
{
    public interface IEnterpriseService
    {
        MultipleEnterprisesResult MultipleEnterprisesResult { get; set; }
        SingleEnterpriseResult SingleEnterpriseResult { get; set; }
        Task<bool> GetEnterprises(long? id, EnterpriseFilter filter, List<Enterprise> investorEnterprises);
    }
}
