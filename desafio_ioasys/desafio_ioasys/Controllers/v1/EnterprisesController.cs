﻿using desafio_ioasys.Models.EntityModels;
using desafio_ioasys.Models.EntityModels.Investors;
using desafio_ioasys.Models.ViewModels.v1;
using desafio_ioasys.Services.Enterprises;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_ioasys.Controllers.v1
{
    [ApiController, Route("api/v1/[controller]")]
    public class EnterprisesController : ControllerBase
    {
        private readonly ApiDbContext _dbContext;
        private readonly HttpContext _httpContext;
        private readonly IEnterpriseService _enterprisesService;

        public EnterprisesController(ApiDbContext dbContext, IHttpContextAccessor httpContextAccessor, IEnterpriseService enterpriseService)
        {
            _dbContext = dbContext;
            _httpContext = httpContextAccessor.HttpContext;
            _enterprisesService = enterpriseService;
        }

        [HttpGet("{id?}")]
        public async Task<IActionResult> Get([FromRoute] long? id, [FromQuery] EnterpriseFilter filter)
        {
            var investorUid = _httpContext.Request.Headers["uid"].ToString();

            if (string.IsNullOrWhiteSpace(investorUid))
            {
                return Forbid();
            }

            var investorEnterprises = _dbContext.Investors
                    .IncludeLogin()
                    .WhereEmail(investorUid)
                    .IncludeEnterprises()
                    .Single()
                    .Enterprises
                    .ToList();


            if (await _enterprisesService.GetEnterprises(id: id, filter: filter, investorEnterprises: investorEnterprises))
            {
                if (_enterprisesService.MultipleEnterprisesResult != null)
                    return Ok(_enterprisesService.MultipleEnterprisesResult);
                else
                    return Ok(_enterprisesService.SingleEnterpriseResult);
            }
            else
                return NotFound();
        }
    }
}
