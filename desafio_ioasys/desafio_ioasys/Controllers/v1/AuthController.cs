﻿using desafio_ioasys.Models.EntityModels;
using desafio_ioasys.Models.EntityModels.Investors;
using desafio_ioasys.Models.EntityModels.InvestorsLogins;
using desafio_ioasys.Models.ViewModels.v1;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SignInResult = desafio_ioasys.Models.ResultModels.Successes.v1.SignInResult;

namespace desafio_ioasys.Controllers.v1
{
    [ApiController, Route("api/v1/users/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly ApiDbContext _dbContext;

        public AuthController(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost("sign_in")]
        public async Task<IActionResult> Auth([FromBody] LoginModel model)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                var investorLogin = await _dbContext.InvestorsLogins
                    .WhereEmail(model.Email)
                    .IncludeInvestor()
                    .SingleOrDefaultAsync();

                if (investorLogin == null)
                    return NotFound();
                else
                {
                    var passwordHash = GetMd5Hash(md5Hash, model.Password);

                    if (passwordHash == investorLogin.Password)
                    {
                        var investor = await _dbContext.Investors
                            .WhereId(investorLogin.InvestorId)
                            .IncludeEnterprises()
                            .IncludeLogin()
                            .SingleAsync();
                        
                        Response.Headers.Add("uid", investorLogin.Email);
                        Response.Headers.Add("access-token", "xxxxxxx");
                        Response.Headers.Add("client", "xxxxxxx");

                        return Ok(new SignInResult(investor));
                    }
                    else
                        return NotFound();
                }
            }
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
