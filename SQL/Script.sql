BEGIN TRAN

INSERT INTO InvestorsLogins (InvestorId, Email, Password) VALUES (1, 'testeapple@ioasys.com.br', 'ed2b1f468c5f915f3f1cf75d7068baae')
INSERT INTO EnterprisesTypes (Name) VALUES ('Software')
GO

INSERT INTO Investors (Name, City, Country, FirstAccess, SuperAngel, LoginId, Balance) VALUES ('Investidor teste', 'Aracaju', 'Brasil', 0, 0, 1, 0)

INSERT INTO Enterprises(Name, [Description], Email, Phone, [OwnerId], City, Country, TypeId) 
VALUES
('Queen Software', 'Startup no ramo de desenvolvimento de soluções em tecnologia', 'queen.software@email.com', '5579951232312', 1, 'Aracaju', 'Brasil', 1)

INSERT INTO Enterprises(Name, [Description], Email, Phone, [OwnerId], City, Country, TypeId) 
VALUES
('Black Sabbath Software', 'Startup no ramo de desenvolvimento de soluções em tecnologia', 'black.software@email.com', '5579951232512', 1, 'Aracaju', 'Brasil', 1)
GO

COMMIT